import React, { Component } from 'react';
import Router from './Router';
class App extends Component {
    constructor(properties) {
        super(properties);
    }
    
    render() {
        return (
            <Router />
        );
    }
}


export default App;
