import React from 'react';
import { Scene, Router, Stack } from 'react-native-router-flux';
import Main from './components/Main';
import List from './components/List';
const RouterComponent = () => {
    return (
        <Router >
            <Stack key="root">
                <Scene key="main" component={Main} title="MAIN" hideNavBar type="replace" /> 
                <Scene key="list" component={List} title="List" hideNavBar /> 
            </Stack>
        </Router>
    );
};
export default RouterComponent;