import { Actions } from 'react-native-router-flux';
import { View, TextInput, Text, Dimensions, Spinner, TouchableOpacity, ScrollView, KeyboardAvoidingView } from 'react-native';
import React, { Component } from 'react';
import moment from 'moment-timezone';
import database from '@react-native-firebase/database';
class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
        loading: false,
        name: "",
        email: "",
        question1: "",
        question2: "",
        question3: "",
        question4: "",
        question4: "",
    };
  }	

  componentDidMount() {
    
  }

  onButtonPress() {
    if(this.state.name === ""){
        this.refs.name.focus();
    }else if(this.state.email === ""){
        this.refs.email.focus();
    }else if(this.state.question1 === ""){
        this.refs.question1.focus();
    }else if(this.state.question2 === ""){
        this.refs.question2.focus();
    }else if(this.state.question3 === ""){
        this.refs.question3.focus();
    }else if(this.state.question4 === ""){
        this.refs.question4.focus();
    }else if(this.state.question5 === ""){
        this.refs.question5.focus();
    }else {
        this.setState({loading: true})
        const question = {
            name: this.state.name,
            email: this.state.email,
            question1: "วันนี้คุณรู้สึกอย่างไง",
            question2: "วันนี้คุณอยากทำอะไรมากที่สุด",
            question3: "วันนี้คุณไม่ชอบอะไรมากที่สุด",
            question4: "พรุ่งนี้คุณอยากทำอะไรมากที่สุด",
            question5: "กิจกรรมที่อยากทำมากที่สุด",
            answer1: this.state.question1,
            answer2: this.state.question2,
            answer3: this.state.question3,
            answer4: this.state.question4,
            answer5: this.state.question5,
            updated: moment().tz("Asia/Bangkok").format('YYYY-MM-DD HH:mm:ss')
        };
        database().ref().child('questions').push(question);
        this.setState({loading: false});
        Actions.list();
    }
  }
  
  renderButton() {
    if (this.state.loading) {
        return <Spinner size="large" />;
    }
    return (
        <TouchableOpacity onPress={this.onButtonPress.bind(this)} style={styles.buttonLoginStyle}>
            <Text style={styles.textButtonStyle}> ส่งแบบสอบถาม </Text>
        </TouchableOpacity>
    );
  }

  render() {
    const winHeight = Dimensions.get('window').height;
      return (
        <View style={{ flex: 1, backgroundColor: '#530068' }}>
            <KeyboardAvoidingView
                    behavior="padding"
                >
                <ScrollView scrollEnabled={true} keyboardShouldPersistTaps='always'>
                    <View style={{ flex: 1, flexDirection: 'column' }} >
                        <View style={{ width: null, height: winHeight }}>
                            <View style={{ width: null, alignItems: 'center' }}>
                                <Text style={{ backgroundColor: 'transparent', marginTop: 80 }}>
                                    <Text style={styles.textWelcomeStyle} >
                                        แบบสอบถาม
                                    </Text>
                                </Text>
                            </View>
                            <Text style={{ backgroundColor: 'transparent', fontSize: 20, color: '#FFFFFF',  marginLeft: 43, marginTop: 25 }}>
                                {'ชื่อ-นามสุกล'}
                            </Text>
                            <View style={styles.containerStyle}>
                                <TextInput 
                                    ref="name"
                                    style={styles.inputStyle} 
                                    keyboardType='default'
                                    autoCapitalize='none'
                                    autoCorrect={false}
                                    placeholder={"กรุณากรอกชื่อ-นามสุกล"}
                                    placeholderTextColor='#9486CB'
                                    underlineColorAndroid='#FFFFFF'
                                    selectionColor='#95989A'
                                    returnKeyType='done'
                                    onChangeText={name => this.setState({ name })}
                                />
                            </View>
                            <Text style={{ backgroundColor: 'transparent', fontSize: 20, color: '#FFFFFF', marginLeft: 43, marginTop: 25 }}>
                                {'อีเมล์'}
                            </Text>
                            <View style={styles.containerStyle}>
                                <TextInput 
                                    ref="email"
                                    style={styles.inputStyle} 
                                    keyboardType='email-address'
                                    autoCapitalize='none'
                                    autoCorrect={false}
                                    placeholder={"กรุณากรอกอีเมล์"}
                                    placeholderTextColor='#9486CB'
                                    underlineColorAndroid='#FFFFFF'
                                    selectionColor='#95989A'
                                    returnKeyType='done'
                                    onChangeText={email => this.setState({ email })}
                                />
                            </View>
                            <View style={{ width: null, marginLeft: 41 }}>
                                <Text style={{ backgroundColor: 'transparent', marginTop: 15 }}>
                                    <Text style={styles.textQuestionStyle} >
                                        คำถาม
                                    </Text>
                                </Text>
                            </View>
                            <Text style={{ backgroundColor: 'transparent', fontSize: 20, color: '#FFFFFF',  marginLeft: 43, marginTop: 25 }}>
                                {'1. วันนี้คุณรู้สึกอย่างไง'}
                            </Text>
                            <View style={styles.containerStyle}>
                                <TextInput 
                                    ref="question1"
                                    style={styles.inputStyle} 
                                    keyboardType='default'
                                    autoCapitalize='none'
                                    autoCorrect={false}
                                    placeholder={"กรุณากรอกคำตอบ"}
                                    placeholderTextColor='#9486CB'
                                    underlineColorAndroid='#FFFFFF'
                                    selectionColor='#95989A'
                                    returnKeyType='done'
                                    onChangeText={question1 => this.setState({ question1 })}
                                />
                            </View>
                            <Text style={{ backgroundColor: 'transparent', fontSize: 20, color: '#FFFFFF',  marginLeft: 43, marginTop: 25 }}>
                                {'2. วันนี้คุณอยากทำอะไรมากที่สุด'}
                            </Text>
                            <View style={styles.containerStyle}>
                                <TextInput 
                                    ref="question2"
                                    style={styles.inputStyle} 
                                    keyboardType='default'
                                    autoCapitalize='none'
                                    autoCorrect={false}
                                    placeholder={"กรุณากรอกคำตอบ"}
                                    placeholderTextColor='#9486CB'
                                    underlineColorAndroid='#FFFFFF'
                                    selectionColor='#95989A'
                                    returnKeyType='done'
                                    onChangeText={question2 => this.setState({ question2 })}
                                />
                            </View>
                            <Text style={{ backgroundColor: 'transparent', fontSize: 20, color: '#FFFFFF',  marginLeft: 43, marginTop: 25 }}>
                                {'3. วันนี้คุณไม่ชอบอะไรมากที่สุด'}
                            </Text>
                            <View style={styles.containerStyle}>
                                <TextInput 
                                    ref="question3"
                                    style={styles.inputStyle} 
                                    keyboardType='default'
                                    autoCapitalize='none'
                                    autoCorrect={false}
                                    placeholder={"กรุณากรอกคำตอบ"}
                                    placeholderTextColor='#9486CB'
                                    underlineColorAndroid='#FFFFFF'
                                    selectionColor='#95989A'
                                    returnKeyType='done'
                                    onChangeText={question3 => this.setState({ question3 })}
                                />
                            </View>
                            <Text style={{ backgroundColor: 'transparent', fontSize: 20, color: '#FFFFFF',  marginLeft: 43, marginTop: 25 }}>
                                {'4. พรุ่งนี้คุณอยากทำอะไรมากที่สุด'}
                            </Text>
                            <View style={styles.containerStyle}>
                                <TextInput 
                                    ref="question4"
                                    style={styles.inputStyle} 
                                    keyboardType='default'
                                    autoCapitalize='none'
                                    autoCorrect={false}
                                    placeholder={"กรุณากรอกคำตอบ"}
                                    placeholderTextColor='#9486CB'
                                    underlineColorAndroid='#FFFFFF'
                                    selectionColor='#95989A'
                                    returnKeyType='done'
                                    onChangeText={question4 => this.setState({ question4 })}
                                />
                            </View>
                            <Text style={{ backgroundColor: 'transparent', fontSize: 20, color: '#FFFFFF',  marginLeft: 43, marginTop: 25 }}>
                                {'5. กิจกรรมที่อยากทำมากที่สุด'}
                            </Text>
                            <View style={styles.containerStyle}>
                                <TextInput 
                                    ref="question5"
                                    style={styles.inputStyle} 
                                    keyboardType='default'
                                    autoCapitalize='none'
                                    autoCorrect={false}
                                    placeholder={"กรุณากรอกคำตอบ"}
                                    placeholderTextColor='#9486CB'
                                    underlineColorAndroid='#FFFFFF'
                                    selectionColor='#95989A'
                                    returnKeyType='done'
                                    onChangeText={question5 => this.setState({ question5 })}
                                />
                            </View>
                            <View style={{ marginTop: 20, marginLeft: 43, marginRight: 42 }}>
                                {this.renderButton()}
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        </View>
      );
    }
}
const styles = {
    textWelcomeStyle: {
        paddingRight: 5, 
        fontSize: 24, 
        color: '#FFFFFF',
        backgroundColor: 'transparent'
    },
    textQuestionStyle: {
        paddingRight: 5, 
        fontSize: 22, 
        color: '#FFFFFF',
        backgroundColor: 'transparent'
    },
    inputStyle: {
        color: '#fff',
        paddingRight: 5,
        paddingLeft: 5,
        marginLeft: 41,
        marginRight: 44,
        fontSize: 20,
        lineHeight: 23,
        flex: 2,
        ...Platform.select({
            ios: {
  
                borderBottomWidth: 1,
                borderBottomColor: '#fff'
            }
        })
    },
    containerStyle: {
        height: 40
    },
    buttonLoginStyle: {
        height: 52,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
        borderRadius: 8
    },
    textButtonStyle: {
        fontSize: 20,
        color: '#464646',
    }
};

export default Main;
