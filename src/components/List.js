import { Actions } from 'react-native-router-flux';
import { View, FlatList, Text, ActivityIndicator } from 'react-native';
import React, { Component } from 'react';
import moment from 'moment-timezone';
import database from '@react-native-firebase/database';
class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
        loading: false,
        dataList: []
    };
  }	

  componentDidMount() {
    const dataList = [];
    database().ref(`/questions`).orderByChild('updated').once('value', (snapshot) => {
        snapshot.forEach((childSnapshot) => {
            var _data = childSnapshot.val();
            _data.key = childSnapshot.key;
            dataList.push(_data);
        });
        this.setState({ dataList });
    });
  }

  renderFeedRow(item, index) {
    return (
    <View style={{ width: null, marginLeft: 24, marginRight: 24, marginBottom: 22 }}>
        <View style={styles.styleFeedRowContent}>
            <View style={{ marginTop: 3, marginLeft: 24, marginRight: 24 }}>
                <Text numberOfLines={1} ellipsizeMode='tail'>
                    <Text style={styles.styleFontBoldRenderSubject}>{item.name} ({item.email}) </Text>
                </Text> 
                <Text numberOfLines={2} ellipsizeMode='tail'>
                    <Text style={styles.styleFontRegularRenderSubject}>{item.updated}</Text> 
                </Text> 
                <Text numberOfLines={3} ellipsizeMode='tail'>
                    <Text style={styles.styleFontRegularRenderSubject}>{item.question1}: {item.answer1}</Text> 
                </Text> 
                <Text numberOfLines={4} ellipsizeMode='tail'>
                    <Text style={styles.styleFontRegularRenderSubject}>{item.question2}: {item.answer2}</Text> 
                </Text> 
                <Text numberOfLines={5} ellipsizeMode='tail'>
                    <Text style={styles.styleFontRegularRenderSubject}>{item.question3}: {item.answer3}</Text> 
                </Text> 
                <Text numberOfLines={6} ellipsizeMode='tail'>
                    <Text style={styles.styleFontRegularRenderSubject}>{item.question4}: {item.answer4}</Text> 
                </Text> 
                <Text numberOfLines={7} ellipsizeMode='tail'>
                    <Text style={styles.styleFontRegularRenderSubject}>{item.question5}: {item.answer5}</Text> 
                </Text> 
            </View>
        </View>
    </View>
    );
}

render() {
  return (
    <View style={{ flex: 1, backgroundColor: '#530068' }}>
        <View style={{ width: null, alignItems: 'center' }}>
            <Text style={{ backgroundColor: 'transparent', marginTop: 80 }}>
                <Text style={styles.textWelcomeStyle}>
                    ข้อมูลทั้งหมด
                </Text>
            </Text>
        </View>
        <View style={{ marginTop: 10, backgroundColor: 'transparent', flex: 1, borderColor: 'transparent' }}>
            <FlatList
                  style={{ backgroundColor: 'transparent', marginTop: 20 }}
                  data={this.state.dataList}
                  renderItem={({ item, index }) => (
                    this.renderFeedRow(item, index)
                  )}
                  keyExtractor={item => item.key}
                  ListFooterComponent={this.renderFooter}
                  refreshing={false}
              />
        </View>    
    </View>
  );
}
}

const styles = {
    textWelcomeStyle: {
        paddingRight: 5, 
        fontSize: 24, 
        color: '#FFFFFF',
        backgroundColor: 'transparent'
    },
    styleFeedRowContent: {
        height: 160, 
        borderColor: '#FFFFFF', 
        borderWidth: 1,
        borderRadius: 6, 
        flexDirection: 'row', 
        alignItems: 'center'
    },
    styleFontRegularRenderSubject: {
        fontSize: 14, 
        color: '#FFFFFF'
    },
    styleFontBoldRenderSubject: {
        fontSize: 18, 
        color: '#FFFFFF'
    },
};

export default List;
